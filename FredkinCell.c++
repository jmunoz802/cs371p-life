#include "FredkinCell.h"
#include <iostream>
#include <cassert>

using namespace std;

FredkinCell::FredkinCell(int x, int y, bool state) : AbstractCell(x,y, state) { _age = 0; }

FredkinCell::FredkinCell(const FredkinCell& that) : AbstractCell(that) , _age(that._age){}

bool FredkinCell::ofAge() const{
	if(_age == 2)
		return true;
	return false;
}

void FredkinCell::markup(const vector<FredkinCell>& neighbor){
	int count = 0, i, size;
	switch(neighbor.size()){
		case 3: size = 2;break;
		case 5: size = 3;break;
		case 8: size = 4;break;
		default: break;
	}
	for(i =0; i < size; i++){
		FredkinCell cell = neighbor[i];
		count += cell.isAlive();
	}

	assert(count < 5);

	//dead->life odd live neihbors
	if(!isAlive() && count % 2 == 1)
		marked();
	//live->dead even neighbors
	if(isAlive() && count % 2 == 0)
		marked();
}

void FredkinCell::update(){
	//life->death
	if(_mark && _state){
		changeState();
	}
	//life->life
	else if(!_mark && _state)
		_age++;
	//death->life
	else if(_mark && !_state)
		changeState();
	
	_mark = false;
}

void FredkinCell::printSelf(){
	if(_state){
		if(_age > 9)
			cout << "+";
		else
			cout<<_age;
	}
	else
		cout<<"-";
}

FredkinCell* FredkinCell::clone() const{
	return new FredkinCell(*this);
}
