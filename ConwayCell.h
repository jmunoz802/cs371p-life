#ifndef conwaycell_h
#define conwaycell_h

#include "AbstractCell.h"
#include <vector>

using namespace std;

class ConwayCell : public AbstractCell {

	public:	
		/*
		 *	constructor takes in x, y int and bool for alive/
		 *	dead state
		 *	@param int, int, bool
		 */ 
		ConwayCell(int, int, bool);

		/*
		 *	Copy Constructor given abstractCell. used for abstract Cell type
		 * 	@param const AbstractCell&
		 */ 	
		ConwayCell(const AbstractCell&);

		/*
		 * 	virtual function that marks the cell for birth/death
		 * 	 by checking neighborhood given
		 * 	@param const vector<ConwayCell>&
		 */ 
		virtual void markup(const vector<ConwayCell>&);

		/*
		 *	updates the state of the function if has been marked
		 */ 
		virtual void update();

		/*
		 *	prints the cell to console 
		 */ 
		virtual void printSelf();

		/*
		 *	Used for dynamic binding, clone returns self as ConwayCell
		 */ 
		virtual ConwayCell* clone() const;
};

#endif
