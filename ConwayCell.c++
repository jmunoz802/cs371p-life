#include "ConwayCell.h"
#include <iostream>

using namespace std;

ConwayCell::ConwayCell(int x, int y, bool state = false) : AbstractCell(x,y, state){}

ConwayCell::ConwayCell(const AbstractCell& that) : AbstractCell(that){}

void ConwayCell::markup(const vector<ConwayCell>& neighbor){
	int count = 0;
	for(ConwayCell cell : neighbor){
		if(cell._state)
			count++;
	}
	if(!isAlive() && count ==3)
		marked();
	if(isAlive() && (count < 2 || count >3))
		marked();
}

void ConwayCell::update(){
	//life->death or death->life
	if(_mark)
		changeState();
	_mark = false;
}

void ConwayCell::printSelf(){
	if(_state)
		cout<<"*";
	else
		cout<<".";
}

ConwayCell* ConwayCell::clone() const{
	return new ConwayCell(*this);
}
