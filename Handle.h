#ifndef handle1_h
#define handle1_h

#include <algorithm>

template<typename T>
class Handle{
	public:
        	typedef T                 value_type;

        	typedef value_type*       pointer;
        	typedef const value_type* const_pointer;

		typedef value_type&       reference;
		typedef const value_type& const_reference;

	private:
        	pointer _p;

protected:
        pointer get () {
            return _p;}

        const_pointer get () const {
            return _p;}

    public:
        Handle (pointer p) {
            _p = p;}

        Handle (const Handle& that) {
            if (!that._p)
                _p = 0;
            else
                _p = that._p->clone();}

        ~Handle () {
            delete _p;}

        Handle& operator = (Handle that) {
            swap(that);
            return *this;}

        void swap (Handle& that) {
            std::swap(_p, that._p);}

};

#endif
