#ifndef fredkincell_h
#define fredkincell_h

#include "AbstractCell.h"
#include <vector>

using namespace std;

class FredkinCell : public AbstractCell{

	int _age; 	//age of cell after surviving generations

	public:
		/*
		 * Constructor takes in x,y coordinates and state of cell being created
		 * @param int, int, bool
		 */
		FredkinCell(int, int, bool);

		/*
		 * Copy constructor 
		 * @param const FredkinCell&
		 */
		FredkinCell(const FredkinCell&);

		/*
		 * Returns true or false depending on the age of fredkin. For use in dynamic binding to mutate Fredkin to ConwayCell
		 * @return bool
		 */
		bool ofAge() const;

		/*
		 * Marks cell for update depending on neighbors given
		 * @param const vector<FredkinCell>&
		 */
		virtual void markup(const vector<FredkinCell>&);

		/*
		 * changes the cell state if it has been marked
		 */
		virtual void update();

		/*
		 * Prints out the age or state of the cell to console
		 */
		virtual void printSelf();

		/*
		 * Clone returns self as FredkinCell for dynamic binding
		 * @return FredkinCell*
		 */
		virtual FredkinCell* clone() const;
};

#endif
