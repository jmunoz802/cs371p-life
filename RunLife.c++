// -------------------------
// projects/life/RunLife.c++
// Copyright (C) 2013
// Glenn P. Downing
// -------------------------

/*
To run the program:
    % g++ -pedantic -std=c++0x -Wall AbstractCell.c++ Cell.c++ ConwayCell.c++ FredkinCell.c++ -o RunLife
    % valgrind RunLife > RunLife.out

To configure Doxygen:
    doxygen -g
That creates the file Doxyfile.
Make the following edits:
    EXTRACT_ALL            = YES
    EXTRACT_PRIVATE        = YES
    EXTRACT_STATIC         = YES
    GENERATE_LATEX         = NO

To document the program:
    doxygen Doxyfile
*/

// --------
// includes
// --------

#include <cassert>   // assert
#include <iostream>  // cout, endl
#include <stdexcept> // invalid_argument, out_of_range
#include <fstream>
#include "Life.h"
#include "ConwayCell.h"
#include "FredkinCell.h"
#include "Cell.h"

// ----
// main
// ----

int main () {
    using namespace std;
    ios_base::sync_with_stdio(false); // turn off synchronization with C I/O

    try {

				ifstream conwayin("RunLifeConway.in" , ifstream::in);
				Life<ConwayCell> Clife;
				while(readLife(conwayin, Clife)){
					Clife.printTitle("ConwayCell");
					Clife.printGrid();
					Clife.simulate(1, true);
					Clife.simulate(4, true);
					Clife.simulate(5, true);
					Clife.simulate(90, true);
					Clife = Life<ConwayCell>();
				}
				conwayin.close();
        }
    catch (const invalid_argument&) {
        assert(false);}
    catch (const out_of_range&) {
        assert(false);}

    try {
	
				ifstream fredkinin("RunLifeFredkin.in" , ifstream::in);
				Life<FredkinCell> Flife;
				while(readLife(fredkinin, Flife)){
					Flife.printTitle("FredkinCell");
					Flife.printGrid();
					int i;
					for(i = 1; i <= 5; i++)
						Flife.simulate(1, true);
					Flife = Life<FredkinCell>();
				}
				fredkinin.close();
				}
    catch (const invalid_argument&) {
        assert(false);}
    catch (const out_of_range&) {
        assert(false);}

    try {

	ifstream Cellin("RunLifeCell.in" , ifstream::in);
	Life<Cell> Cell_life;
	while(readLife(Cellin, Cell_life)){
		Cell_life.printTitle("Cell");
		Cell_life.printGrid();

		int i;
		for(i = 1; i <= 5; i++)
			Cell_life.simulate(1, true);
	
		Cell_life = Life<Cell>();
	}
	Cellin.close();
        }
    catch (const invalid_argument&) {
        assert(false);}
    catch (const out_of_range&) {
        assert(false);}

    return 0;}
