#include "AbstractCell.h"
AbstractCell::AbstractCell(int x = 0, int y = 0, bool state = false): _x(x), _y(y), _state(state){ _mark = false;}

AbstractCell& AbstractCell::operator=(const AbstractCell& that){
	_x = that._x;
	_y = that._y;
	_state = that._state;
	_mark = false;
	return *this;
}

void AbstractCell::changeState(){
	_state = !_state;
}

int AbstractCell::isAlive(){
	if(_state)
		return 1;
	return 0;
}

void AbstractCell::marked(){
	_mark = true;
}

AbstractCell* AbstractCell::clone() const{
	return new AbstractCell(*this);
}

void AbstractCell::update(){
	return;
}

void AbstractCell::printSelf(){
	return;
}

void AbstractCell::markup(const vector<AbstractCell>& neighbor){
	return;
}
