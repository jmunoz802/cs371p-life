var searchData=
[
  ['_5fage',['_age',['../classFredkinCell.html#a2486375e903e4fec0a43d1b183fc1098',1,'FredkinCell']]],
  ['_5fgen',['_gen',['../classLife.html#a1416c42186fccd22085a0bf89a77a074',1,'Life']]],
  ['_5fgrid',['_grid',['../classLife.html#a1bbe491cac194c10869e81f57cbe707e',1,'Life']]],
  ['_5fmark',['_mark',['../classAbstractCell.html#a7c1a52b82e1cc386c08adf2f2ef0d4de',1,'AbstractCell']]],
  ['_5fp',['_p',['../classHandle.html#ad74f33e968f57f03dd7e6a2be10b8cd2',1,'Handle']]],
  ['_5fpopulation',['_population',['../classLife.html#a0b071f31ac08e3a754490f6a767e1ae8',1,'Life']]],
  ['_5fstate',['_state',['../classAbstractCell.html#a3bb489f0bc1aa3c925bca073b89c28be',1,'AbstractCell']]],
  ['_5fx',['_x',['../classAbstractCell.html#aba8cb742f256bd5b3e3f383284ff819d',1,'AbstractCell::_x()'],['../classLife.html#a3aada04c2036057bd828def8a629349e',1,'Life::_x()']]],
  ['_5fy',['_y',['../classAbstractCell.html#ad2c4a8e6190f2fbf48d6ca99dc652ea5',1,'AbstractCell::_y()'],['../classLife.html#ae66903990101aa9391315ffa82b8fdad',1,'Life::_y()']]]
];
