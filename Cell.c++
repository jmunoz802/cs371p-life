#include "Cell.h"
#include "FredkinCell.h"
#include "ConwayCell.h"
#include <vector>
#include <iostream>
#include "Handle.h"

using namespace std;

Cell::Cell() : Handle(0){}

Cell::Cell(int x, int y, bool b) : Handle(new FredkinCell(x,y,b)){}

void Cell::markup(const vector<Cell>& neighbor){

	int count = 0, i, size;

	if(FredkinCell* const f = dynamic_cast<FredkinCell*>(get())){

		 switch(neighbor.size()){
			case 3: size = 2;break;
			case 5: size = 3;break;
			case 8: size = 4;break;
			default: break;
		}
		for(i =0; i < size; i++){
			Cell cell = neighbor[i];
			count += cell.isAlive();
		}

		//dead->life odd live neihbors
		if(!f->isAlive() && count % 2 == 1)
			f->marked();
		//live->dead even neighbors
		if(f->isAlive() && count % 2 == 0)
			f->marked();

	}
	else{
		ConwayCell* const c = dynamic_cast<ConwayCell*>(get());
		for(Cell cell : neighbor){
    	count += cell.isAlive();
    }
    if(!isAlive() && count ==3)
    	c->marked();
		if(isAlive() && (count < 2 || count >3))
			c->marked();

	}
}

void Cell::mutate(){
	Handle swp(new ConwayCell(*(get())));
	swap(swp);
}

void Cell::update(){
	get()->update();
	if(const FredkinCell* const f = dynamic_cast<FredkinCell*>(get())){
		if(f->ofAge()){
			mutate();
		}
	}
}

int Cell::isAlive(){
	return get()->isAlive();
}

void Cell::printSelf(){
	get()->printSelf();
}

void Cell::changeState(){
	get()->changeState();
}
