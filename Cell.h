#ifndef cell_h
#define cell_h

#include "Handle.h"
#include <vector>
#include "AbstractCell.h"

using namespace std;

class Cell : Handle<AbstractCell> {

	public:
		/*
		 *	Default constructor setting Handle<T> _p to 0
		 */
		Cell();

		/*
		 *	Constructor to create FredkinCell base given x,y, bool state
		 * 	@param int, int, bool
		 */
		Cell(int, int, bool);

		/*
		 *	Returns 1 if alive 0 otherwise. Information on Cell's current state
		 * 	@return int
		 */
		int isAlive();

		/*
		 *	Marks Cell for death/birth depending on neighbors
		 * 	@param const vector<Cell>&
		 */
		void markup(const vector<Cell>&);

		/*
		 *	Changes FredkinCell to ConwayCell when cell becomes correct age
		 */
		void mutate();

		/*
		 *	Changes state of current cell
		 */
		void update();

		/*
		 *	prints cell out to console
		 */
		void printSelf();

		/*
		 *	changes the cell from live to dead and vice versa
		 */
		void changeState();

};

#endif
