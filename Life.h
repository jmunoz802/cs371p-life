#ifndef life_h
#define life_h

#include <vector>
#include <iostream>
#include <string.h>
#include <cassert>

using namespace std;

template <typename T>
class Life{

	vector<T> _grid;	//contains cells in 1D row-major order vector
	int _x;		//amount of rows
	int _y;		//amount of columns
	int _gen;	//generation
	int _population;	//number of live cells

	public:

			/*
			 *	Constructor that takes in board size, and initializes values
			 *	@param x, y
			 */
			Life(int x = 1, int y = 1) : _x(x), _y(y){
				_gen = 0;
				_population = 0;
			}

			/*
			 *	Helper function that places cells at given x,y location
			 *	@param x, y, T cell
			 */
			void placeCell(int x, int y, T& cell){
				_grid[x*_y+y] = cell;
			}

			/*
			 *	returns cell from given x, y coordinates
			 *	@param x, y
			 */
			T& getCell(int x, int y){
				return _grid[x*_y+y];
			}

			/*
			 *	Prints to console the title of the current board
			 *	@param string
			 */
			void printTitle(string type){
				cout << "***Life<" << type << "> " << _x << "x" << _y <<" ***" <<endl;
			}

			/*
			 *	Prints entire board with generation number and population
			 */
			void printGrid(){
				cout << "Generation = " << _gen << " Population = " << _population << endl;
				int i = 1;
				for(T cell : _grid){
					cell.printSelf();
					if(i%_y==0)
						cout <<endl;
					i++;
				}
				cout <<endl;
			}

			/*
			 *	Main function that marks and updates cells. Runs for given cycle
			 *  and prints if bool arg is true
			 *	@param cycles, bool print
			 */
			void simulate(int cycles, bool print=false){
				
				int i;
				for(i=1; i <= cycles; i++){
						//markup corner cells
						vector<T> neighbor;
						neighbor.push_back(getCell(0,1));
						neighbor.push_back(getCell(1,0));
						neighbor.push_back(getCell(1,1));
						getCell(0,0).markup(neighbor);
						neighbor.clear();

						neighbor.push_back(getCell(0, _y-2));
						neighbor.push_back(getCell(1, _y-1));
						neighbor.push_back(getCell(1, _y-2));
						getCell(0, _y-1).markup(neighbor);
						neighbor.clear();

						neighbor.push_back(getCell(_x-1, 1));
						neighbor.push_back(getCell(_x-2, 0));
						neighbor.push_back(getCell(_x-2, 1));
						getCell(_x-1, 0).markup(neighbor);
						neighbor.clear();

						neighbor.push_back(getCell(_x-1, _y-2));
						neighbor.push_back(getCell(_x-2, _y-1));
						neighbor.push_back(getCell(_x-2, _y-2));
						getCell(_x-1, _y-1).markup(neighbor);
						neighbor.clear();

						//handle border cases
						//top bottom border
						vector<T> botright;
						int i,j;
						for(i = 1 ; i < _y-1; i++){
							neighbor.push_back(getCell( 0, i+1));
							neighbor.push_back(getCell( 0, i-1));
							neighbor.push_back(getCell( 1, i));
							neighbor.push_back(getCell( 1, i-1));
							neighbor.push_back(getCell( 1, i+1));

							botright.push_back(getCell( _x-1, i+1));
							botright.push_back(getCell( _x-1, i-1));
							botright.push_back(getCell( _x-2, i));
							botright.push_back(getCell( _x-2, i-1));
							botright.push_back(getCell( _x-2, i+1));

							getCell(0,i).markup(neighbor);//toprow
							getCell( _x-1, i).markup(botright);//bottomrow
							neighbor.clear();
							botright.clear();
						}
						//left right borders
						for(i = 1 ; i < _x-1; i++){
							neighbor.push_back(getCell( i+1, 0));
							neighbor.push_back(getCell( i-1, 0));
							neighbor.push_back(getCell( i, 1));
							neighbor.push_back(getCell( i-1, 1));
							neighbor.push_back(getCell( i+1, 1));

							botright.push_back(getCell( i+1, _y-1));
							botright.push_back(getCell( i-1, _y-1));
							botright.push_back(getCell( i, _y-2));
							botright.push_back(getCell( i-1, _y-2));
							botright.push_back(getCell( i+1, _y-2));

							getCell(i,0).markup(neighbor);
							getCell(i, _y-1).markup(botright);
							neighbor.clear();
							botright.clear();
						}

						//interior cells loop
						for(i = 1; i < _x-1; i++){
								for(j = 1; j < _y-1; j++){
									neighbor.push_back(getCell(i-1 ,j ));//laterals
									neighbor.push_back(getCell(i+1 ,j ));
									neighbor.push_back(getCell(i ,j+1 ));
									neighbor.push_back(getCell(i ,j-1 ));
									neighbor.push_back(getCell(i-1 ,j-1 ));//diagonals
									neighbor.push_back(getCell(i-1 ,j+1 ));
									neighbor.push_back(getCell(i+1 ,j+1 ));
									neighbor.push_back(getCell(i+1 ,j-1 ));
									getCell(i,j).markup(neighbor);
									neighbor.clear();
								}
						}
						_gen++;
						_population = 0;
						for(i = 0; i < _x; i++){
							int j;
							for(j = 0; j < _y; j++){
								T& cell = getCell(i,j);
								cell.update();
								_population +=cell.isAlive();
							}
						}
				}			
				if(print)
					printGrid();	
			}		

			/*
			 *	reads in next Life board and returns true/false if data is read
			 *	correctly
			 *	@param istream, Life<U>
			 * 	@return bool
			 */
			template<typename U>
			bool friend readLife(istream& in, Life<U>& life);
};

template<typename T>
bool readLife(istream& in, Life<T>& life){

	in >> life._x;

	if(!in)
		return false;

	in >> life._y;
	assert(life._x > 0 && life._y > 0);
	
	string buf;
	getline(in,buf);
	int i;
	for(i = 0; i < life._x; i++){
		getline(in, buf);
		int j = 0;
		for( char c : buf ){
			//create cell, default dead
			T cell(i, j, false);
			switch(c){
				//flip the state to alive
				case '-':
				case '.':
					break;
				default:
					cell.changeState();
					life._population++;
					break;
			}
			//place cells in grid
			life._grid.push_back(cell);
			j++;
		}
	}
	getline(in, buf);
	return true;
}

#endif
