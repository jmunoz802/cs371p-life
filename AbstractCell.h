#ifndef abstractCell_h
#define abstractCell_h

#include <vector>

using namespace std;

class AbstractCell{
	protected:
		int _x;
		int _y;
		bool _state;
		bool _mark;

		AbstractCell(int, int, bool);
	public:
		AbstractCell& operator=(const AbstractCell&);
		void changeState();
		int isAlive();
		void marked();
		virtual AbstractCell* clone() const;
		virtual void markup(const vector<AbstractCell>&);
		virtual void update();
		virtual void printSelf();
};

#endif
